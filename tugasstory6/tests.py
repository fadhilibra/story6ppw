from django.test import TestCase, Client
from django.urls import resolve
from tugasstory6.views import bikinstatus, redirecting
from django.http import HttpRequest
from tugasstory6.models import Status
from tugasstory6.forms import StatusForm


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
import time


# Create your tests here.

class UnitTestStory6(TestCase):
	def test_tugasstory6_url_is_exist(self):
		response = Client().get('/statusku/')
		self.assertEqual(response.status_code, 200)

	def test_notexist_url_is_notexist(self):
		response = Client().get('/gaada/')
		self.assertEqual(response.status_code, 404)

	def test_tugasstory6_using_bikinstatus_function(self):
		response = resolve('/statusku/')
		self.assertEqual(response.func, bikinstatus)

	def test_tugasstory6_using_tugasstory6_template(self):
		response = Client().get('/statusku/')
		self.assertTemplateUsed(response, 'statusgue.html')

	def test_landing_page_is_completed(self):
		request = HttpRequest()
		response = bikinstatus(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Halo, apa kabar?', html_response)

	def test_landing_page_title_is_right(self):
		request = HttpRequest()
		response = bikinstatus(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<title>STATUS GUEEE</title>', html_response)

	#Redirect landing page
	def test_landing_page_using_redirecting_function(self):
		response = resolve('/')
		self.assertEqual(response.func, redirecting)

	def test_landing_page_redirecting(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 302)

	def test_landing_page_redirected_to_statusku(self):
		response = Client().get('/')
		self.assertRedirects(response, '/statusku/')
    

	#SetUp and Models
	def setUp(cls):
		Status.objects.create(status="coba-coba")

	def test_if_models_in_database(self):
		modelsObject = Status.objects.create(status="Halaaah")
		count_Object = Status.objects.all().count()
		self.assertEqual(count_Object, 2)

	def test_if_Status_status_is_exist(self):
		modelsObject = Status.objects.get(id=1)
		statusObj = Status._meta.get_field('status').verbose_name
		self.assertEqual(statusObj, 'status')

	#Forms
	def test_forms_input_html(self):
		form = StatusForm()
		self.assertIn('id="id_status', form.as_p())
    
	def test_forms_validation_blank(self):
		form = StatusForm(data={'status':''})
		self.assertFalse(form.is_valid())
		self.assertEquals(form.errors['status'], ["This field is required."])

	#forms in templates
	def test_forms_in_template(self):
		request = HttpRequest()
		response = bikinstatus(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<form method="POST" class="register_form">', html_response)


class FunctionalTestStory6(TestCase):

    def setUp(self):
	    chrome_options = webdriver.ChromeOptions()
	    chrome_options.add_argument('--dns-prefetch-disable')
	    chrome_options.add_argument('--no-sandbox')
	    chrome_options.add_argument('--headless')
	    chrome_options.add_argument('disable-gpu')
	    self.selenium = webdriver.Chrome(chrome_options=chrome_options)
	    super(FunctionalTestStory6, self).setUp()


    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTestStory6, self).tearDown()

    def test_input_todo(self):
	    selenium = self.selenium
	    # Opening the link we want to test
	    selenium.get('http://127.0.0.1:8000/statusku/')
	    # find the form element
	    isi = selenium.find_element_by_id("id_status")
	    submit = selenium.find_element_by_id("button")
	
	    isi.send_keys("Coba Coba")
	    submit.send_keys(Keys.RETURN)
	    time.sleep(3)
	    self.assertIn("Coba Coba", selenium.page_source)



from django.shortcuts import render,redirect
from tugasstory6.models import Status
from tugasstory6.forms import StatusForm

# Create your views here.

def bikinstatus(request):
	if request.method == "POST":
		form = StatusForm(request.POST)
		if form.is_valid():
			data = form.save()
			data.save()
			return redirect('/statusku/')
	else:
		form = StatusForm()

	isistatus = Status.objects.order_by('-date')
	return render(request, 'statusgue.html', {'form': form, 'status': isistatus})

def redirecting(request):
	return redirect('/statusku/')


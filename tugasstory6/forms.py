from django import forms
from .models import Status


class StatusForm(forms.ModelForm):
    status = forms.CharField(max_length=300, widget=forms.Textarea)
    class Meta:
    	model = Status
    	fields = ('status',)
